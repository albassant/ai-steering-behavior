﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Steering
{
    public class BlendedSteering : SteeringBehaviour
    {
        public struct BehaviourAndWeight
        {
            public SteeringBehaviour Behaviour;
            public float Weight;
        }

        public List<BehaviourAndWeight> Behaviours = new List<BehaviourAndWeight>();

        public override SteeringOutput GetSteering()
        {
            var steering = new SteeringOutput();
            steering.Linear = Vector2.zero;
            steering.Angular = 0f;

            foreach (var behaviour in Behaviours)
            {
                behaviour.Behaviour.Character = this.Character;

                var steer = behaviour.Behaviour.GetSteering();
                steering.Linear += behaviour.Weight * steer.Linear;
                steering.Angular += behaviour.Weight * steer.Angular;
            }

            float acceleration = steering.Linear.magnitude;
            steering.Linear.Normalize();
            steering.Linear *= Mathf.Min(acceleration, Character.Parameters.MaxAcceleration);

            steering.Angular = Mathf.Min(steering.Angular, Character.Parameters.MaxAngularAcceleration);

            return steering;
        }
    }

    public class PriotitySteering : SteeringBehaviour
    {
        public List<BlendedSteering> Groups = new List<BlendedSteering>();

        static float EPSILON = 0.001f;

        public override SteeringOutput GetSteering()
        {
            var steering = new SteeringOutput();
            steering.Linear = Vector2.zero;
            steering.Angular = 0f;

            foreach (var group in Groups)
            {
                group.Character = this.Character;

                steering = group.GetSteering();

                if (steering.Linear.sqrMagnitude > EPSILON || Mathf.Abs(steering.Angular) > EPSILON)
                    return steering;
            }
            return steering;
        }
    }
}
