﻿using UnityEngine;
using System.Collections;

namespace Steering
{
    public struct SteeringOutput
    {
        public Vector2 Linear;
        public float Angular;
    }

    public class StaticObject
    {
        public Vector2 Position = Vector2.zero;
        public float Orientation = 0f;

        public void FromUnityTransform(Transform transform)
        {
            Position = transform.position;
            Orientation = Mathf.Deg2Rad * (transform.rotation.eulerAngles.z);
        }

        public Transform ToUnityTransform(Transform trmToRefresh)
        {
            trmToRefresh.position = Position;
            Vector3 euler = trmToRefresh.rotation.eulerAngles;
            euler.z = Orientation * Mathf.Rad2Deg;
            trmToRefresh.rotation = Quaternion.Euler(euler);
            return trmToRefresh;
        }
    }

    public class KinematicObject : StaticObject
    {
        public Vector2 Velocity = Vector2.zero;
        public float Rotation = 0f;

        [System.Serializable]
        public struct SpeedParameters
        {
            public float MaxSpeed;
            public float MaxAcceleration;
            public float MaxAngularSpeed;
            public float MaxAngularAcceleration;

            public float MaxSpeedSqr
            {
                get { return MaxSpeed * MaxSpeed; }
            }

            public float MaxAccelerationSqr
            {
                get { return MaxAcceleration * MaxAcceleration; }
            }
        }

        public SpeedParameters Parameters;

        public KinematicObject(SpeedParameters param)
        {
            Parameters = param;
        }

        public void Update(SteeringOutput steering, float time)
        {
            Position += Velocity * time;
            Orientation += Rotation * time;

            Velocity += steering.Linear * time;
            Rotation += steering.Angular * time;

            if (Velocity.sqrMagnitude > Parameters.MaxSpeedSqr)
            {
                Velocity.Normalize();
                Velocity *= Parameters.MaxSpeed;
            }

            if (Mathf.Abs(Rotation) > Parameters.MaxAngularSpeed)
            {
                Rotation /= Mathf.Abs(Rotation);
                Rotation *= Parameters.MaxAngularSpeed;
            }
        }
    }
}
