﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Steering
{
    public class Separation : SteeringBehaviour
    {
        public List<KinematicObject> Targets = new List<KinematicObject>();

        [System.Serializable]
        public struct Parameters
        {
            public float Threshold;
            public float DecayCoefficient;
        }

        Parameters separationParameters;

        public Separation(Parameters param)
        {
            separationParameters = param;
        }

        public override SteeringOutput GetSteering()
        {
            var steering = new SteeringOutput();
            steering.Linear = Vector2.zero;
            steering.Angular = 0;

            foreach (var target in Targets)
            {
                if (target == Character)
                    continue;

                var direction = Character.Position - target.Position;
                var distance = direction.sqrMagnitude;

                if (distance < separationParameters.Threshold * separationParameters.Threshold)
                {
                    var strength = Mathf.Min(separationParameters.DecayCoefficient / distance, Character.Parameters.MaxAcceleration);
                    direction.Normalize();
                    steering.Linear += strength * direction;
                }
            }

            return steering;
        }
    }
}
