﻿using UnityEngine;

namespace Steering
{
    public abstract class SteeringBehaviour
    {
        public KinematicObject Character { get; set; }
        public StaticObject Target { get; set; }

        public abstract SteeringOutput GetSteering();
    }

    public class Seek : SteeringBehaviour
    {
        public override SteeringOutput GetSteering()
        {
            var steering = new SteeringOutput();
            steering.Linear = Target.Position - Character.Position;

            steering.Linear.Normalize();
            steering.Linear *= Character.Parameters.MaxAcceleration;

            steering.Angular = 0;
            return steering;
        }
    }
    
    public class Arrive : SteeringBehaviour
    {
        [System.Serializable]
        public struct Parameters
        {
            public float TargetRadius;
            public float SlowRadius;
            public float TimeToTarget;
        }

        Parameters arriveParameters;

        public Arrive(Parameters param)
        {
            arriveParameters = param;
        }

        public override SteeringOutput GetSteering()
        {
            var steering = new SteeringOutput();

            var direction = Target.Position - Character.Position;
            var distance = direction.magnitude;

            if (distance < arriveParameters.TargetRadius)
            {
                Character.Velocity = Vector2.zero;
                steering.Linear = Vector2.zero;
                return steering;
            }

            var targetSpeed = (distance > arriveParameters.SlowRadius) ? 
                Character.Parameters.MaxSpeed : 
                Character.Parameters.MaxSpeed * distance / arriveParameters.SlowRadius;

            direction.Normalize();
            var targetVelocity = direction * targetSpeed;

            steering.Linear = targetVelocity - Character.Velocity;
            steering.Linear /= arriveParameters.TimeToTarget;

            if (steering.Linear.sqrMagnitude > Character.Parameters.MaxAccelerationSqr)
            {
                steering.Linear.Normalize();
                steering.Linear *= Character.Parameters.MaxAcceleration;
            }

            steering.Angular = 0;
            return steering;
        }
    }

    public class Align : SteeringBehaviour
    {
        [System.Serializable]
        public struct Parameters
        {
            public float TargetRadius;
            public float SlowRadius;
            public float TimeToTarget;
        }

        Parameters alignParameters;

        public Align(Parameters param)
        {
            alignParameters = param;
        }

        public override SteeringOutput GetSteering()
        {
            var steering = new SteeringOutput();

            var rotation = Target.Orientation - Character.Orientation;
            rotation = MapRotationToRange(rotation);
            var rotationSize = Mathf.Abs(rotation);
            
            if (rotationSize < alignParameters.TargetRadius)
            {
                Character.Rotation = 0f;
                steering.Angular = 0f;
                return steering;
            }

            float targetRotation = rotationSize > alignParameters.SlowRadius ? 
                Character.Parameters.MaxAngularSpeed : 
                Character.Parameters.MaxAngularSpeed * rotationSize / alignParameters.SlowRadius;

            targetRotation *= rotation / rotationSize;

            steering.Angular = targetRotation - Character.Rotation;
            steering.Angular /= alignParameters.TimeToTarget;

            var angularAcceleration = Mathf.Abs(steering.Angular);
            if (angularAcceleration > Character.Parameters.MaxAngularAcceleration)
            {
                steering.Angular /= angularAcceleration;
                steering.Angular *= Character.Parameters.MaxAngularAcceleration;
            }

            steering.Linear = Vector2.zero;
            return steering;
        }

        public float MapRotationToRange(float rotation)
        {
            if (rotation > Mathf.PI)
                return rotation - 2f * Mathf.PI;
            else if (rotation < -Mathf.PI)
                return rotation + 2f * Mathf.PI;
            return rotation;
        }
    }
}
 