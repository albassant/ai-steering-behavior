﻿using UnityEngine;

namespace Steering
{
    public class Face : Align
    {
        public Face(Parameters param) : base(param) { }

        StaticObject delegateTarget = new StaticObject();
        StaticObject targetCached;

        static float EPSILON = 0.001f;

        public new StaticObject Target
        {
            get { return targetCached; }
            set { targetCached = value; }
        }

        public override SteeringOutput GetSteering()
        {
            if (Target != null)
            {
                var direction = Target.Position - Character.Position;
                delegateTarget.Orientation = (direction.sqrMagnitude < EPSILON) ?
                    Character.Orientation :
                    Mathf.Atan2(direction.y, direction.x);
            }
            else
            {
                delegateTarget.Orientation = Character.Orientation;
            }

            base.Target = delegateTarget;
            return base.GetSteering();
        }
    }

    public class LookWhereYoureGoing : Align
    {
        public LookWhereYoureGoing(Parameters param) : base(param) { }

        static float EPSILON = 0.001f;

        public override SteeringOutput GetSteering()
        {
            Target.Orientation = Character.Velocity.sqrMagnitude < EPSILON ?
                 Character.Orientation :
                 Mathf.Atan2(Character.Velocity.y, Character.Velocity.x);
            return base.GetSteering();
        }
    }
}
