﻿using UnityEngine;
using System.Collections.Generic;

public class FieldManager : MonoBehaviour
{
    public event System.Action<Vector2> OnTargetUpdated;
    public event System.Action<KinematicObjectController> OnObjectAdded;
    public List<KinematicObjectController> KinematicObjects = new List<KinematicObjectController>();

    [SerializeField]
    KinematicObjectController FieldObjectPrefab;

    #region Input handlers
    public void HandleUpdateTarget(Vector2 pos, bool isHolding)
    {
        if (OnTargetUpdated != null)
            OnTargetUpdated(pos);
    }

    public void HandleAddFieldObject(Vector2 pos)
    {
        KinematicObjectController koc = GameObject.Instantiate<KinematicObjectController>(FieldObjectPrefab);
        koc.transform.position = pos;
        koc.transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(1f, 360f));

        KinematicObjects.Add(koc);

        if (OnObjectAdded != null)
            OnObjectAdded(koc);
    }
    #endregion
}
