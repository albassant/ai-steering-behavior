﻿using UnityEngine;
using System.Collections;

public class FieldInputHandler : MonoBehaviour
{
    InputController inputController;
    FieldManager fieldManager;

    void Start()
    {
        fieldManager = GetComponent<FieldManager>();

        inputController = GameObject.FindObjectOfType<InputController>();
        if (inputController == null)
            Debug.LogError("No input controller found!");

        inputController.OnLeftClick += HandleLeftClick;
        inputController.OnRightClick += HandleRightClick;
    }
	
    void HandleLeftClick(Vector2 pos, bool isDragging)
    {
        fieldManager.HandleUpdateTarget(pos, isDragging);
    }

    void HandleRightClick(Vector2 pos)
    {
        fieldManager.HandleAddFieldObject(pos);
    }

    void OnDestroy()
    {
        if (inputController != null)
        {
            inputController.OnLeftClick -= HandleLeftClick;
            inputController.OnRightClick -= HandleRightClick;
        }
    }
}
