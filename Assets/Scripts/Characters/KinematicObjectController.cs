﻿using UnityEngine;
using Steering;
using System.Collections.Generic;

public class KinematicObjectController : MonoBehaviour
{
    public KinematicObject.SpeedParameters SpeedParameters;
    public Arrive.Parameters ArriveParameters;
    public Align.Parameters AlignParameters;
    public Separation.Parameters SeparationParameters;

    KinematicObject steeringObject;
    StaticObject targetObject;
    FieldManager fieldManager;

    public KinematicObject SteeringObject { get { return steeringObject; } }

    List<KinematicObject> neighbours = new List<KinematicObject>();

    Arrive move;
    LookWhereYoureGoing rotation;
    Separation separation;
    BlendedSteering steering;

    void Awake()
    {
        steeringObject = new KinematicObject(SpeedParameters);
    }

    void Start()
    {
        InitializeSteeringData();

        fieldManager = GameObject.FindObjectOfType<FieldManager>();
        if (fieldManager != null)
        {
            fieldManager.OnTargetUpdated += HandleTargetUpdated;
            fieldManager.OnObjectAdded += HandleKinematicObjectAdded;

            if (fieldManager.KinematicObjects.Count > 0)
            {
                foreach (var koc in fieldManager.KinematicObjects)
                {
                    neighbours.Add(koc.SteeringObject);
                }
                separation.Targets = neighbours;
            }
        }
    }

    void InitializeSteeringData()
    {
        steeringObject.FromUnityTransform(transform);

        move = new Arrive(ArriveParameters);
        rotation = new LookWhereYoureGoing(AlignParameters);
        separation = new Separation(SeparationParameters);

        steering = new BlendedSteering();
        steering.Behaviours.Add(new BlendedSteering.BehaviourAndWeight() { Behaviour = move, Weight = 1f });
        steering.Behaviours.Add(new BlendedSteering.BehaviourAndWeight() { Behaviour = rotation, Weight = 1f });
        steering.Behaviours.Add(new BlendedSteering.BehaviourAndWeight() { Behaviour = separation, Weight = 2f });

        steering.Character = steeringObject;
    }

    void HandleTargetUpdated(Vector2 pos)
    {
        if (targetObject == null)
        {
            targetObject = new StaticObject();
            move.Target = rotation.Target = targetObject;
        }

        targetObject.Position = pos;
    }

    void HandleKinematicObjectAdded(KinematicObjectController koc)
    {
        Debug.Log(koc.SteeringObject);
        neighbours.Add(koc.SteeringObject);
    }

    void Update()
    {
        if (targetObject != null)
        {
            steeringObject.Update(steering.GetSteering(), Time.deltaTime);

            var newTransform = steeringObject.ToUnityTransform(transform);
            transform.position = newTransform.position;
            transform.rotation = newTransform.rotation;
        }
    }

    void OnDestroy()
    {
        if (fieldManager != null)
        {
            fieldManager.OnTargetUpdated -= HandleTargetUpdated;
            fieldManager.OnObjectAdded -= HandleKinematicObjectAdded;
        }
    }
}
