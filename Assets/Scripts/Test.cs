﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

    public AnimationCurve curve;
    public Vector3 target;
    public float time = 5.0f;
	
	IEnumerator Start ()
    {
        float curTime = 0.0f;
        Vector3 start = this.transform.position;

        Vector3 direction = (target - start);

        while (curTime < time)
        {
            curTime += Time.deltaTime;
            float t = curTime / time;

            Vector3 positionNow = start + direction * curve.Evaluate(t);
            this.transform.position = positionNow;

            yield return null;
        }
	}
	
	void Update ()
    {
	
	}
}
