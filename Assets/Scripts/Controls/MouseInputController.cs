﻿using UnityEngine;
using System.Collections;

public class MouseInputController : InputController
{
    Vector2 firstClickPos;
    Vector2 secondClickPos;

    protected override void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            OnRightClick(World(Input.mousePosition));
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                firstClickPos = Input.mousePosition;
                OnLeftClick(World(firstClickPos), false);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                return;
            }
            else if (Input.GetMouseButton(0))
            {
                secondClickPos = Input.mousePosition;
                currentDrag = new Vector2(secondClickPos.x - firstClickPos.x, secondClickPos.y - firstClickPos.y);
                firstClickPos = secondClickPos;

                if (currentDrag.sqrMagnitude < minDragLength)
                    return;

                OnLeftClick(World(secondClickPos), true);
            }
        }
    }
}
