﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    public System.Action<Vector2, bool> OnLeftClick;
    public System.Action<Vector2> OnRightClick;

    protected Vector2 currentDrag;
    public float minDragLength = 5f;

    protected virtual void Update()
    {
    }

    public Vector3 World(Vector3 screen)
    {
        return Camera.main.ScreenToWorldPoint(screen);
    }
}
